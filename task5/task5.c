#include <stdio.h>
#include <time.h>

int main(int argc, char** argv[])
{
  time_t t = time(NULL);
  tm* aTm = localtime(&t);
  printf("%04d/%02d/%02d %02d:%02d:%02d \n",aTm->tm_year+1900, aTm->tm_mon+1, aTm->tm_mday, aTm->tm_hour, aTm->tm_min, aTm->tm_sec);
  getchar();
  return 0;
}