#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 256
#define MIN_AGE 18
#define MAX_AGE 100
#define MIN_TIME 8
#define MAX_TIME 22
void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);

}
int main()
{
	char name[N];
	int t[3];
	int age = 0;

	puts("Hello, what is your name?");
	fgets(name, N, stdin);
	if (name[strlen(name) - 1] == '\n')
		name[strlen(name) - 1] = 0;
	
  	while (1)
	{
		puts("What's time now?");
		if (scanf("%d", &t) == 1)
		{
			if (t >= MIN_TIME && t <= MAX_TIME)
				puts("It is beartime");
			else
				puts("it is not your time");
			break;
		}
		else
			puts("Input error!");
		clean_stdin();
	}
	while (1)
	{
		printf("How old are you, %s?\n", name);
		if (scanf("%d", &age) == 1)
		{
			if (_strcmpi(name, "michael") == 0 ||
				(age >= MIN_AGE && age <= MAX_AGE))
				puts("Access granted");
			else
				puts("Access denied");
			break;
		}
		else
			puts("Input error!");
		clean_stdin();
	}
	return 0;
}